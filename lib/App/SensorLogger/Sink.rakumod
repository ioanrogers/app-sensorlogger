use v6;
unit role App::SensorLogger::Sink;
need App::SensorLogger::Reading;

multi method write(App::SensorLogger::Reading $) { ... }
multi method write(App::SensorLogger::Reading @) { ... }

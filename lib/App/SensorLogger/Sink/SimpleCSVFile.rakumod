use App::SensorLogger::Sink;

unit class App::SensorLogger::Sink::SimpleCSVFile
  does App::SensorLogger::Sink;

has IO::Path(Str) $.csv-file = "{$*PROGRAM.basename}.{$*PID}.csv";

multi method write(App::SensorLogger::Reading $reading) {
    given $!csv-file.IO.open(:a) {
        .lock;
        .say: $reading;
        .close;
    }
}

multi method write(App::SensorLogger::Reading @readings) {
    given $!csv-file.IO.open(:a) {
        .lock;
        .say: @readings;
        .close;
    }
}

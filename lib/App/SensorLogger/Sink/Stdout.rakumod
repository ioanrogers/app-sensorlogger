use v6.d;
use App::SensorLogger::Sink;

unit class App::SensorLogger::Sink::Stdout
  does App::SensorLogger::Sink;

#= Whether to prepend a timestamp to the log output
has Bool $.show-timestamp = False;

multi method write(App::SensorLogger::Reading $reading) {
    if $!show-timestamp {
        say $reading;
    } else {
        say "sensor={$reading.name} {$reading.type}={$reading.value.fmt: '%.2f'}"
    }
}

multi method write(App::SensorLogger::Reading @readings) {
    .say for @readings;
}

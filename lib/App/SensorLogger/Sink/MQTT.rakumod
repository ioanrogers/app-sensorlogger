use v6.d;

need App::SensorLogger::Sink;

unit class App::SensorLogger::Sink::MQTT:ver<0.0.1>:auth<gitlab:ioanrogers>
  does App::SensorLogger::Sink;

use Net::MQTT;
use JSON::Fast;

has Str $.host is required;

has $!mqtt-client = Net::MQTT.new(
    :server($!host),
);

multi method write(App::SensorLogger::Reading $reading) {
    my $json = to-json { $reading.type => $reading.value };
    await $!mqtt-client.publish("homeassistant/sensor/{$reading.name}-{$reading.type}/state", $json, :retain);
}

multi method write(App::SensorLogger::Reading @readings) {
    for @readings -> $r {
        my $json = to-json { $r.type => $r.value };
        await $!mqtt-client.publish("homeassistant/sensor/{$r.name}-{$r.type}/state", $json, :retain);
    }
}

=begin pod

=head1 NAME

App::SensorLogger::Sink::MQTT - blah blah blah

=head1 SYNOPSIS

=begin code :lang<raku>

use App::SensorLogger::Sink::MQTT;

=end code

=head1 DESCRIPTION

App::SensorLogger::Sink::MQTT is ...

=head1 AUTHOR

Ioan Rogers <ioan@rgrs.ca>

=head1 COPYRIGHT AND LICENSE

Copyright 2021 Ioan Rogers

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod

use v6;
unit class App::SensorLogger::Reading;

has DateTime $.timestamp = .now.utc;

has Str $.name is required;

has Str $.type is required;

has Rat $.value is required;

multi method gist(App::SensorLogger::Reading:D:) {
    "{$.timestamp.truncated-to: 'second'} sensor=$.name $.type={$.value.fmt: '%.2f'}"
}

use v6.d;

unit class App::SensorLogger:ver<0.0.1>:auth<gitlab:ioanrogers>;
use YAMLish;
need App::SensorLogger::Reading;

has Bool $.debug = False;
has Str $.config-path is required;

#| Sensors will be read once per C<$.frequency-duration>
has Duration $.frequency-duration = .new: 300;

has @!sensors;
has @!sinks;

method get-sensors-config(@sensors-config) returns Array {
    my @sensors;
    say @sensors.elems;
    for @sensors-config -> $sensor-config {

        if $sensor-config<protocol>.uc ne 'I2C' {
            die "Unhandled protocol type: $sensor-config<protocol>";
        }

        my $sensor-module = sprintf 'Device::%s::Sensor::%s',
                $sensor-config<protocol>.uc, $sensor-config<chip>.uc;

        # TODO no reason there couldn't be multiple sensors of the same type attached
        # so skip loading already loaded modules
        try require ::($sensor-module);
        if ::($sensor-module) ~~ Failure {
            die "Failed to load $sensor-module: $!";
        }

        # TODO maybe debug flag per sensor?
        my %args = :$!debug, i2c-bus-device-path => $sensor-config<bus-path>;

        %args<i2c-device-address> = $sensor-config<addr> if $sensor-config<addr>;
        my $sensor = ::($sensor-module).new(|%args);

        # TODO don't actually talk to the sensor yet, makes testing harder
        # $sensor.reset if $sensor.^can('reset');

        my $senses = $sensor.get-sensor-features;

        # TODO generate some kind of of id for the sensor, e.g. node-proto-chip-(counter|address)
        say "Configured sensor: $sensor-config<chip>";
        @sensors.push: {
            config => $sensor-config,
            instance => $sensor,
            :$senses,
        };

    }

    return @sensors;
}

method get-sinks-config(@sinks-config) returns Array {
    my @sinks;

    for @sinks-config -> $sink-config {
        my $sink-module = "App::SensorLogger::Sink::$sink-config<sink>";

        # TODO no reason there couldn't be multiple sinks of the same type
        # so skip loading already loaded modules
        try require ::($sink-module);
        if ::($sink-module) ~~ Failure {
            die "Failed to load $sink-module: $!";
        }

        my $sink;
        if $sink-config<options> {
            $sink = ::($sink-module).new(|$sink-config<options>);
        } else {
            $sink = ::($sink-module).new;
        }
        say "Configured sink: $sink-config<sink>";
        @sinks.push: $sink;
    }

    return @sinks;
}

method read-config {
    say "Loading config from $!config-path";

    my %config = load-yaml($!config-path.IO.slurp);
    if %config<frequency-duration>:exists {
        $!frequency-duration = Duration.new: %config<frequency-duration>;
    }

    # TODO node name defaults to hostname?
    say "Setting up sensor node %config<node>";
    @!sensors = self.get-sensors-config(%config<sensors>);
    @!sinks = self.get-sinks-config(%config<sinks>);
}

method run() {

    self.read-config;
    say "Reading sensors every {$!frequency-duration}s";

    signal(SIGTERM, SIGINT).tap(-> $sig {
        say "Received $sig, shutting down.";
        exit;
    });

    loop {
        for @!sensors -> $s {
            for $s<senses>.values -> $sense {
                my $reading = App::SensorLogger::Reading.new(
                        name => $s<config><chip>,
                        type => $sense,
                        value => $s<instance>."get-$sense"(),
                        );
                # TODO handle failures in case of multiple sinks. e.g. if a remote
                # sink goes down, we should continue writing local logs
                .write($reading) for @!sinks;
            }
        }

        sleep $!frequency-duration;
    }
}

=begin pod

=head1 NAME

App::Sensor::Logger - blah blah blah

=head1 SYNOPSIS

=begin code :lang<raku>

use App::Sensor::Logger;

=end code

=head1 DESCRIPTION

App::Sensor::Logger is ...

=head1 AUTHOR

Ioan Rogers <ioan@rgrs.ca>

=head1 COPYRIGHT AND LICENSE

Copyright 2021 Ioan Rogers

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod

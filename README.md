NAME
====

App::Sensor::Logger - blah blah blah

SYNOPSIS
========

```raku
use App::Sensor::Logger;
```

DESCRIPTION
===========

App::Sensor::Logger is ...

AUTHOR
======

Ioan Rogers <ioan@rgrs.ca>

COPYRIGHT AND LICENSE
=====================

Copyright 2021 Ioan Rogers

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.


#!/usr/bin/env raku

use v6.d;
use App::SensorLogger;

for $*OUT, $*ERR {
    .out-buffer = False unless .t;
}

sub MAIN(
        Bool  :$debug,      #= Enable debug output
        Str:D :$config-path #= Path to config file
         ) {
    App::SensorLogger.new(:$debug, :$config-path).run;
}

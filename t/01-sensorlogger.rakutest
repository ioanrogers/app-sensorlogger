use Test;

need App::SensorLogger;

throws-like { App::SensorLogger.new }, X::Attribute::Required;
ok my $app = App::SensorLogger.new(config-path => 'eg/config.yaml'), "Create a new sensorlogger";
isa-ok $app, App::SensorLogger;

subtest {
    isa-ok $app.frequency-duration, Duration, '.frequency-duration is a duration...';
    is     $app.frequency-duration, 300, '...is 300s';
#    lives-ok { $app.read-config }, 'read config';
#    is     $app.frequency-duration, 120, '...is now 120s';
}, "Frequency Duration Config";

subtest {
    my @sensor-config =
            { :protocol<i2c>, :bus-path</dev/i2c-3>, :chip<htu21d> },
            { :protocol<i2c>, :bus-path</dev/i2c-3>, :chip<mcp9808> };

    ok (my @sensors = $app.get-sensors-config: @sensor-config), 'handled sensor config';

    isa-ok @sensors, Array, 'is an array';
    is     @sensors.elems, 2, 'with 2 elements';
    isa-ok @sensors[0], Hash, 'element is an hash';
    is     @sensors[0]<config><chip>, 'htu21d', 'correct chip';
    isa-ok @sensors[0]<instance>, 'Device::I2C::Sensor::HTU21D', 'device module was loaded';
}, "Sensors loaded";

subtest {
    ok my @sinks = $app.get-sinks-config: [{ :sink<Stdout> },];

    isa-ok @sinks, Array, 'is an array';
    is     @sinks.elems, 1, 'with 1 element';
    isa-ok @sinks[0], 'App::SensorLogger::Sink::Stdout', 'containing a Sink::Stdout instance';
    is     @sinks[0].show-timestamp, False, 'no timestamp';
}, "One sink loaded with no args";

subtest {
    ok my @sinks = $app.get-sinks-config: [{ :sink<Stdout>, options => { :show-timestamp } },];

    isa-ok @sinks, Array, 'is an array';
    is     @sinks.elems, 1, 'with 1 element';
    isa-ok @sinks[0], 'App::SensorLogger::Sink::Stdout', 'containing a Sink::Stdout instance';
    is     @sinks[0].show-timestamp, True, 'with timestamp';
}, "One sink loaded with args";

subtest {
    ok my @sinks = $app.get-sinks-config: [{ :sink<Stdout> }, { :sink<SimpleCSVFile> }, ];

    isa-ok @sinks, Array, 'is an array';
    is     @sinks.elems, 2, 'with 2 elements';
    isa-ok @sinks[0], 'App::SensorLogger::Sink::Stdout', 'contains a Sink::Stdout instance';
    isa-ok @sinks[1], 'App::SensorLogger::Sink::SimpleCSVFile', 'contains a Sink::SimpleCSVFile instance';
}, "Two sink loaded with no args";

subtest {
    ok my @sinks = $app.get-sinks-config: [
        { :sink<Stdout>, options => { :show-timestamp } },
        { :sink<SimpleCSVFile>, options => { :csv-file<test.csv> } },
    ];

    isa-ok @sinks, Array, 'is an array';
    is     @sinks.elems, 2, 'with 2 elements';
    isa-ok @sinks[0], 'App::SensorLogger::Sink::Stdout', 'contains a Sink::Stdout instance';
    is     @sinks[0].show-timestamp, True, 'with timestamp';

    isa-ok @sinks[1], 'App::SensorLogger::Sink::SimpleCSVFile', 'contains a Sink::SimpleCSVFile instance';
    is     @sinks[1].csv-file, 'test.csv', 'with specified csv file';

}, "Two sink loaded with no args";

done-testing;
